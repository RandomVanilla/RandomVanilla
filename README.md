# Random Vanilla

[![Discord Server](https://img.shields.io/discord/668861153518288927.svg?color=blueviolet)](https://discord.gg/4P2dXxF)
[![](https://img.shields.io/badge/api-fabric-orange.svg)](https://www.curseforge.com/minecraft/mc-mods/fabric-api/files)

Random Vanilla is a Minecraft mod that adds random, vanilla-like features!

## Building
1. JDK 8 is required. Install it using https://adoptopenjdk.net
2. Download the [sources](https://gitlab.com/RandomVanilla/RandomVanilla/-/archive/master/RandomVanilla-master.zip) to a folder
2. Open a terminal window in the same directory as the sources (`shift` + `right click` while inside the desired folder and `Open PowerShell window here`) and run `./gradlew build`
3. After some time, the built mod will be in `/build/libs`.

## Installation (Users)
Quazi-Modded uses *Fabric* as it's mod API. Refer to their installation instructions [here](https://fabricmc.net#installation)

Once you have Fabric installed, simply download the latest version of Random Vanilla from [CurseForge](https://curseforge.com/minecraft/mc-mods/randomvanilla/files) and place it in your `mods` folder.
**Remember to use the Fabric launcher profile when starting the game!**
